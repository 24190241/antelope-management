<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\project;
use App\document;

class DocumentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $project = project::findOrFail($id);

        return view('admin/project/document/create', ['project' => $project]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'file' => 'required|mimes:pdf',
        ]);

        $project = $request['project_id'];

        $document = $request->file('file');
        $documentName = date('Y-m-d').'-'.time().'.'.$document->getClientOriginalExtension();
        $target_path    =   public_path('/uploads/documents/');

        $document->move($target_path, $documentName);

        $upload = document::create(['name' => $request['name'], 
                                    'file' => $documentName,
                                    'user_id' => $request['user_id'],
                                    'project_id' => $project]);

        return redirect()->route('project_view', [$project]);
            
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }
    /**
     * View the file within th application
     * @return \Illuminate\Http\Response
     */
    public function download($id) 
    {
        $document = document::findOrFail($id);
        $file = $document['file'];
        $filePath = public_path('/uploads/documents/'.$file);

        return Response()->download($filePath);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $document = document::find($id);
        $project = $document['project_id'];
        $document->delete();
        return redirect()->route('project_view', [$project]);
    }
}
