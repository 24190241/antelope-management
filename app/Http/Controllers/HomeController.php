<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\task;
use App\user_task;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = User::find(2);
        $userTasks = user_task::where('user_id', 2)->get();
        $tasks = task::all();
        return view('admin/home', ['user' => $user, 'userTasks' => $userTasks, 'tasks' => $tasks]);
    }
}
