<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\project;
use App\photo;

class PhotoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $project = project::findOrFail($id);

        return view('admin/project/photo/create', ['project' => $project]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'file' => 'required|mimes:png,jpg,svg,jpeg',
        ]);

        $project = $request['project_id'];

        $photo = $request->file('file');
        $photoName = date('Y-m-d').'-'.time().'.'.$photo->getClientOriginalExtension();
        $target_path    =   public_path('/uploads/photos/');

        $photo->move($target_path, $photoName);

        $upload = photo::create(['name' => $request['name'], 
                                 'file' => $photoName,
                                 'user_id' => $request['user_id'],
                                 'project_id' => $project]);
                                 
        return redirect()->route('project_view', [$project]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($file)
    {
        $photo = photo::findOrFail($file);
        return view('admin/project/photo/display', compact('photo'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $photo = photo::find($id);
        $project = $photo['project_id'];
        $photo->delete();
        return redirect()->route('project_view', [$project]);
    }
}
