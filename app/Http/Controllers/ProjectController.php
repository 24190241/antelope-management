<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\project;
use App\company;
use App\project_type;
use App\employer;
use App\task;
use App\note;
use App\document;
use App\photo;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = project::all();
        $employers = employer::all();

        return view('admin/project/projectDashboard', ['projects' => $projects, 'employers' => $employers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $companies = company::all();
        $projectTypes = project_type::all();
        $employers = employer::all();

        return view('admin/project/create', [
            'companies' => $companies,
            'projectTypes' => $projectTypes,
            'employers' => $employers
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:users',
            'description' => 'required',
            'deadline' => 'required',
            'company_id' => 'required',
            'employer_id' => 'required',
            'project_type_id' => 'required',
        ]);
        $request['completed'] = 0;

        $input = $request->all();

        $project = project::create($input);
        $project->companies()->save($request->input('company_id'));
        $project->employer()->save($request->input('employer_id'));
        $project->project_type()->save($request->input('project_type_id'));


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $project = project::findOrFail($id);
        $tasks = task::all();
        $notes = note::all();
        $documents = document::all();
        $photos = photo::all();

        $employer = employer::where('id', $project['employer_id'])->first();
        $projectType = project_type::where('id', $project['project_type_id'])->first();


        return view('admin/project/display', compact('project', 
                                                     'tasks', 
                                                     'notes', 
                                                     'documents', 
                                                     'photos',
                                                     'employer',
                                                     'projectType'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $project = project::findOrFail($id);
        $companies = company::all();
        $projectTypes = project_type::all();
        $employers = employer::all();

        return view('admin/project/edit', compact('project',
                                                  'companies',
                                                  'projectTypes',
                                                  'employers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $project = project::findOrFail($id);
        $project->update($request->all());
        return redirect('/projects');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $project = project::find($id);
        $project->delete();
        return redirect('/projectDashboard');    
    }
}
