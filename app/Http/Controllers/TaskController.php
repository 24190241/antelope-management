<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\project;
use App\task;
use App\User;
use App\user_task;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $users = User::all();
        $project = project::findOrFail($id);

        return view('admin/project/task/create', ['users' => $users, 'project' => $project]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'description' => 'required',
            'deadline' => 'required',
        ]);

        $project = $request['project_id'];

        $request['completed'] = 0;
        $input = $request->all();

        $task = task::create($input);

        $user_task_data = [
            'user_id' => $request->input('user_id', true),
            'task_id' => $task->id,
        ];
        $user_task = user_task::create($user_task_data);

        return redirect()->route('project_view', [$project]);
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $task = task::findOrFail($id);

        return view('admin/project/task/view', compact('task'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = task::findOrFail($id);
        $users = user::all();
        $project = project::findOrFail($task['project_id']);
        return view('admin/project/task/edit', compact('task', 'users', 'project'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = task::findOrFail($id);

        $project = $request['project_id'];

        $task->update($request->all());
        return redirect()->route('project_view', [$project]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = task::find($id);
        $project = $task['project_id'];
        $task->delete();
        return redirect()->route('project_view', [$project]);
    }
}
