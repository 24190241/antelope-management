<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class company extends Model
{
    protected $fillable = [
        'companyName', 'contactName', 'phoneNumber', 'email',
    ];

    public function projects() {
        return $this->belongsToMany('App\project');
    }
}
