<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class document extends Model
{
    protected $fillable = [
        'name', 'file', 'user_id', 'project_id', 'task_id',
    ];

    public function users() {
        return $this->belongsTo('App\User');
    }
    public function projects() {
        return $this->belongsTo('App\project');
    }
    public function tasks() {
        return $this->belongsTo('App\task');
    }
}
