<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class employer extends Model
{
    protected $fillable = [
        'name',
    ];

    public function users() {
        return $this->belongsToMany('App\User');
    }
    public function projects() {
        return $this->belongsToMany('App\project');
    }
}
