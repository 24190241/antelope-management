<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class photo extends Model
{
    protected $fillable = [
        'name', 'user_id', 'project_id', 'task_id',
    ];

    public function users() {
        return $this->belongsTo('App\User');
    }
    public function projects() {
        return $this->belongsTo('App\project');
    }
    public function tasks() {
        return $this->belongsTo('App\task');
    }
}
