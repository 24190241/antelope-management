<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class project extends Model
{
    protected $fillable = [
        'name', 'description', 'deadline', 'completed', 
        'company_id', 'project_type_id', 'employer_id',
    ];

    public function companies() {
        return $this->hasOne('App\company');
    }
    public function projectTypes() {
        return $this->hasOne('App\projectType');
    }
    public function employers() {
        return $this->hasOne('App\employer');
    }
    public function users() {
        return $this->belongsToMany('App\User');
    }
}
