<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class project_type extends Model
{
    protected $fillable = [
        'name',
    ];

    public function projects() {
        return $this->belongsToMany('App\project');
    }
}
