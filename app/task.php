<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class task extends Model
{
    protected $fillable = [
        'name', 'description', 'deadline', 
        'completed', 'project_id',
    ];

    public function projects() {
        return $this->belongsTo('App\project');
    }
    public function users() {
        return $this->belongsToMany('App\User');
    }
}
