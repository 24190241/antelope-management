<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class user_task extends Model
{
    protected $fillable = [
        'user_id', 'task_id'
    ];

    public function user() {
        return $this->hasMany('users', 'user_id');
    }
    public function task() {
        return $this->hasMany('tasks', 'task_id');
    }
}
