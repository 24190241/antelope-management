<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class user_type extends Model
{
    protected $fillable = [
        'name',
    ];

    public function users() {
        return $this->belongsToMany('App\User');
    }
}
