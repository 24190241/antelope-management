<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name')->unique();
            $table->longText('description');
            $table->date('deadline');
            $table->boolean('completed');
            $table->integer('company_id')->unsigned();
            //$table->foreign('company_id')->references('id')->on('companies')->onDelete('cascade');
            $table->integer('project_type_id')->unsigned();
            //$table->foreign('project_type_id')->references('id')->on('project_types')->onDelete('cascade');
            $table->integer('employer_id')->unsigned();
            //$table->foreign('employerid')->references('id')->on('employers')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
