@extends('layouts.master')

@section('title', 'Admin Dashboard')

@section('content')
<h1>Admin Dashboard</h1>

<table class="halfTable">
    <tr>
        <th>
            <h2>System Users</h2>
            <button><a href="/user/create">Add User</a></button>
        </th>
    </tr>
    @if (isset ($users))
    @foreach ($users as $user)
    <tr>
        <td class="title">
            {{ $user->name }}
            <a class="delete" name="{{$user->id}}" href="{{ route('user.delete', $user->id) }}"><i class="material-icons">delete</i></a>
            <a class="edit" name="{{$user->id}}" href="/user/{{$user->id}}/edit"><i class="material-icons">edit</i></a>
        </td>
    </tr>
    <tr>
        <td>
            <p>Email Address: {{ $user->email }}</p>
            <p>Job Role: {{ $user->jobRole }}</p>
            <p>Employer: {{ $user->employer_id['name'] }} </p>
            <p>User Type: {{ $user->user_type_id['name'] }}</p>
            <p>Password: {{ $user->password }} </p>
        </td>
    </tr>
    @endforeach
    @endif
</table>

<table class="halfTable">
    <tr>
        <th>
            <h2>User Types</h2>
            <button><a href="/userType/create">Add User Type</a></button>
        </th>
    </tr>
    @if (isset ($userTypes))
    @foreach ($userTypes as $userType)
    <tr>
        <td class="title">
            {{ $userType->name }}
            <a class="edit" name="{{$userType->id}}" href="/userType/{{ $userType->id }}/edit"><i class="material-icons">edit</i></a>
        </td>
    </tr>
    @endforeach
    @endif
</table>

<table class="halfTable">
    <tr>
        <th>
            <h2>Employers</h2>
            <button><a href="/employer/create">Add Employer</a></button>
        </th>
    </tr>
    @if (isset ($employers))
    @foreach ($employers as $employer)
    <tr>
        <td class="title">
            {{ $employer->name }}
            <a class="edit" name="{{$employer->id}}" href="/employer/{{$employer->id}}/edit"><i class="material-icons">edit</i></a>
        </td>
    </tr>
    @endforeach
    @endif
</table>

<table class="halfTable">
    <tr>
        <th>
            <h2>Project Types</h2>
            <button><a href="/projectType/create">Add Project Type</a></button>
        </th>
    </tr>
    @if (isset ($projectTypes))
    @foreach ($projectTypes as $projectType)
    <tr>
        <td class="title">
            {{ $projectType->name }}
            <a class="edit projectType" href="/projectType/{{$projectType->id}}/edit"><i class="material-icons">edit</i></a>
        </td>
    </tr>
    @endforeach
    @endif
</table>

@endsection