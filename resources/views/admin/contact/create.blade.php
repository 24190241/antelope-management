@extends('layouts.master')

@section('title', 'Contacts - Add New')

@section('content')
<h1>New Contact</h1>

@if ($errors->any())
    <div class="errorAlert">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{action('CompanyController@store')}}" method="post" class="createContact">
    @csrf 

    <label for="companyName">Company Name:</label>
    <input type="text" name="companyName">

    <label for="contactName">Contact Name:</label>
    <input type="text" name="contactName">

    <label for="phoneNumber">Phone Number:</label>
    <input type="text" name="phoneNumber">

    <label for="email">Email Address:</label>
    <input type="email" name="email">

    <input type="submit" value="Submit">
    <button class="cancelButton"><a href="/contactBook">Cancel</a></button>

</form>
@endsection