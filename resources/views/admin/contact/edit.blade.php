@extends('layouts.master')

@section('title', 'Edit Contact')

@section('content')
<h1>Edit Contact</h1>

@if ($errors->any())
    <div class="errorAlert">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="/contact/{{$company->id}}/update" method="PATCH"  class="editContact">
    @csrf 
    <label for="companyName">Company Name:</label>
    <input type="text" name="companyName" value="{{$company->companyName}}">

    <label for="contactName">Contact Name:</label>
    <input type="text" name="contactName" value="{{$company->contactName}}">

    <label for="phoneNumber">Phone Number:</label>
    <input type="text" name="phoneNumber" value="0{{$company->phoneNumber}}">

    <label for="email">Email Address:</label>
    <input type="email" name="email" value="{{$company->email}}">

    <input type="submit" value="Submit">
    <button class="cancelButton"><a href="/contactBook">Cancel</a></button>

</form>
@endsection