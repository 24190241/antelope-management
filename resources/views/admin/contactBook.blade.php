@extends('layouts.master')

@section('title', 'Contact Book')

@section('content')
<h1>Contact Book</h1>

<table class="halfTable">
    <tr>
        <th>
            <h2>System Users</h2>
        </th>
    </tr>
    @if (isset ($users))
    @foreach ($users as $user)
    <tr>
        <td class="title">
            {{ $user->name }}
        </td>
    </tr>
    <tr>
        <td>
            <p>Job Role: {{ $user->jobRole}}</p>
            <p>Email Address: {{ $user->email }}</p>
        </td>
    </tr>
    @endforeach
    @endif
</table>

<table class="halfTable">
    <tr>
        <th>
            <h2>Companies</h2>
            <button><a href="/contact/create">Add Company</a></button>
        </th>
    </tr>
    @if (isset ($companies))
    @foreach ($companies as $company)
    <tr>
        <td class="title">
            {{ $company->companyName }}
            <a class="delete" name="{{$company->id}}" href="{{ route('company.delete', $company->id) }}"><i class="material-icons">delete</i></a>
            <a class="edit" href="/contact/{{ $company->id }}/edit"><i class="material-icons">edit</i></a>
        </td>
    </tr>
    <tr>
        <td>
            <p>Contact Name: {{ $company->contactName }}</p>
            <p>Phone Number: 0{{ $company->phoneNumber }}</p>
            <p>Email Address: {{ $company->email }}</p>
        </td>
    </tr>
    @endforeach 
    @else 
    <tr>
        <td>
            No Companies added yet
        </td>
    </tr> 
    @endif 
</table>
@endsection