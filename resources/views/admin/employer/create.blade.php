@extends('layouts.master')

@section('title', 'New Employer')

@section('content')
<h1>New Employer</h1>

@if ($errors->any())
    <div class="errorAlert">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form action="{{action('EmployerController@store')}}" method="POST" class="createEmployer">
    @csrf
    
    <label for="name">Employer Name:</label>
    <input type="text" name="name">

    <input type="submit" value="Submit">
    <button class="cancelButton"><a href="/admin">Cancel</a></button>
</form>
@endsection