@extends('layouts.master')

@section('title', 'User Dashboard')

@section('content')
<h1>Hello {{ $user->name }}</h1>

<table class="fullTable">
    <tr>    
        <th>
            <h2>Upcoming Tasks</h2>
        </th>
    </tr>
    @if (isset ($userTasks))
    @foreach ($userTasks as $usertask)
        @if (isset ($tasks))
        @foreach ($tasks as $task)
            @if($task->id == '$userTask->task_id')
                <tr>
                    <td>
                        <a href="/task/{{$task->id}}">{{ $task->name }}</a>
                    </td>
                    <td>
                        {{ $task->deadline }}
                    </td>
                </tr>
            @endif
        @endforeach
        @endif
    @endforeach
    @endif
</table>
@endsection