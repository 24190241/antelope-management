@extends('layouts.master')

@section('title', 'New Project')

@section('content')
<h1>New Project</h1>

@if ($errors->any())
    <div class="errorAlert">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{action('ProjectController@store')}}" method="post" class="createProject">
    @csrf

    <label for="name">Project Name:</label>
    <input type="text" name="name">

    <label for="description">Project Description:</label>
    <textarea name="description"></textarea>

    <label for="deadline">Deadline Date:</label>
    <input type="date" name="deadline">

    <label for="company_id">Company: </label>
    <select name="company_id">
        @if (isset ($companies))
        <option value="">Select...</option>
        @foreach ($companies as $company)
        <option value="{{ $company->id }}">{{ $company->companyName }}</option>
        @endforeach
        @endif
    </select>

    <label for="project_type_id">Project Type</label>
    <select name="project_type_id">
        @if (isset ($projectTypes))
        <option value="">Select...</option>
        @foreach ($projectTypes as $projectType)
        <option value="{{ $projectType->id }}">{{ $projectType->name }}</option>
        @endforeach
        @endif
    </select>

    <label for="employer_id">Company Overseeing the Project:</label>
    <select name="employer_id">
        @if (isset ($employers))
        <option value="">Select...</option>
        @foreach ($employers as $employer)
        <option value="{{ $employer->id }}">{{ $employer->name }}</option>
        @endforeach
        @endif
    </select>

    <input type="submit" value="Submit">
    <button class="cancelButton"><a href="/admin">Cancel</a></button>

</form>
@endsection