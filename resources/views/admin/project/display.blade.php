@extends('layouts.master')

@section('title', 'Projects')

@section('content')
<h1>{{ $project->name }}</h1> 
<div class="details">
    <h3>Over Seen by: {{ $employer->name }}</h3>
    <h3>Deadline to be completed by: {{ $project->deadline }} </h3>
    <h3>Project type: {{ $projectType->name }} </h3>
</div>

<p>{{ $project->description }}</p>

<table class="halfTable">
    <tr>    
        <th>
            <h2>Notes</h2>
            <button><a href="/projects/{{ $project->id }}/note/create">Add Note</a></button>
        </th>
    </tr>
    @if (isset ($notes))
    @foreach ($notes as $note)
    <tr>
        <td class="title">
            {{ $note->user_id }} - {{ $note->created_at }}
            
            <a class="delete" name="{{$note->id}}" href="{{ route('note.delete', $note->id) }}"><i class="material-icons">delete</i></a>
            </form>        
        </td>
    </tr>
    <tr>
        <td>
            {{ $note->note }}
        </td>
    </tr>
    @endforeach
    @endif
</table>

<table  class="halfTable">
    <tr colspan="2">    
        <th>
            <h2>Tasks</h2>
            <button><a href="/projects/{{ $project->id }}/task/create">Add Task</a></button>
        </th>
    </tr>
    @if (isset ($tasks))
    @foreach ($tasks as $task)
    <tr> 
        <td class="title">
            <a href="/task/{{ $task->id }}">{{ $task->name }}</a>
        
            {{ $task->deadline }}
            <a class="delete" name="{{$task->id}}" href="{{ route('task.delete', $task->id) }}"><i class="material-icons">delete</i></a>
            <a class="edit" href="/task/{{$task->id}}/edit"><i class="material-icons">edit</i></a>

        </td>
    </tr>
    @endforeach
    @endif 
</table>

<table class="halfTable">
    <tr>    
        <th>
            <h2>Documents</h2>
            <button><a href="/projects/{{ $project->id }}/document/create">Add Document</a></button>
        </th>
    </tr>
    @if (isset ($documents))
    @foreach ($documents as $document)
    <tr> 
        <td class="title">
            <a href="/document/{{$document->id}}/download">{{ $document->name }}</a>
            <a class="delete" name="{{$document->id}}" href="{{ route('document.delete', $document->id) }}"><i class="material-icons">delete</i></a>
        </td>
    </tr>
    @endforeach
    @endif 
</table>

<table class="halfTable">
    <tr>    
        <th>
            <h2>Photos</h2>
            <button><a href="/projects/{{ $project->id }}/photo/create">Add Photo</a></button>
        </th>
    </tr>
    @if (isset ($photos))
    @foreach ($photos as $photo)
    <tr> 
        <td class="title">
            <a href="/photo/{{$photo->file}}">{{ $photo->name }}</a>
            <a class="delete" name="{{$photo->id}}" href="{{ route('photo.delete', $photo->id) }}"><i class="material-icons">delete</i></a>
        </td>
    </tr>
    @endforeach
    @endif
</table>

@endsection