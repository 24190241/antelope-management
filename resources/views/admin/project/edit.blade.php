@extends('layouts.master')

@section('title', 'Edit Project')

@section('content')
<h1>Edit Project</h1>

@if ($errors->any())
    <div class="errorAlert">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="/projects/{{$project->id}}/update" method="PATCH" class="editProject">
    @csrf

    <label for="name">Project Name:</label>
    <input type="text" name="name" value="{{$project->name}}">

    <label for="description">Project Description:</label>
    <input type="text" name="description" value="{{$project->description}}">

    <label for="deadline">Deadline Date:</label>
    <input type="date" name="deadline" value="{{$project->deadline}}">

    <label for="company_id">Company: </label>
    <select name="company_id">
        @if (isset ($companies))
            @foreach ($companies as $company)
                @if($company->id == '$project->company_id') 
                    <option value="{{$company->id}}" selected>{{$company->companyName}}</option>
                @else 
                    <option value="{{$company->id}}">{{$company->companyName}}</option>
                @endif
            @endforeach
        @endif
    </select>

    <label for="project_type_id">Project Type</label>
    <select name="project_type_id">
        @if (isset ($projectTypes))
            @foreach ($projectTypes as $projectType)
                @if($projectType->id == '$project->projectType_id') 
                    <option value="{{$projectType->id}}" selected >{{$projectType->name}}</option>
                @else 
                    <option value="{{$projectType->id}}">{{$projectType->name}}</option>
                @endif
            @endforeach
        @endif
    </select>

    <label for="employer_id">Company Overseeing the Project:</label>
    <select name="employer_id">
        @if (isset ($employers))
            @foreach ($employers as $employer)
                @if($employer->id == '$project->employer_id') 
                    <option value="{{$employer->id}}" selected >{{$employer->name}}</option>
                @else 
                    <option value="{{$employer->id}}">{{$employer->name}}</option>
                @endif            
            @endforeach
        @endif
    </select>

    <input type="submit" value="Submit">
    <button class="cancelButton"><a href="/projects">Cancel</a></button>

</form>
@endsection