@extends('layouts.master')

@section('title', 'New Note')

@section('content')
<h1>New Note</h1>

@if ($errors->any())
    <div class="errorAlert">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{action('NoteController@store')}}" method="post" class="createNote">
    @csrf
    <label for="note">Note:</label>
    <textarea name="note"></textarea>

    <input style="display: none;" name="project_id" value="{{ $project->id }}">
    <input style="display: none;" name="user_id" value="2">

    <input type="submit" value="Submit">
    <button class="cancelButton"><a href="/projects/{{ $project->id }}">Cancel</a></button>

</form>

@endsection