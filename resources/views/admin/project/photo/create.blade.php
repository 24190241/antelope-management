@extends('layouts.master')

@section('title', 'New Photo')

@section('content')
<h1>New Photo</h1>

@if ($errors->any())
    <div class="errorAlert">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{action('PhotoController@store')}}" method="post" class="createPhoto" enctype="multipart/form-data">
    @csrf

    <label for="name">Name:</label>
    <input type="text" name="name">

    <label for="file">Photo:</label>
    <input type="file" name="file">

    <input style="display: none;" name="project_id" value="{{ $project->id }}">
    <input style="display: none;" name="user_id" value="2">

    
    <input type="submit" value="Submit">
    <button class="cancelButton"><a href="/projects/{{ $project->id }}">Cancel</a></button>
</form>

@endsection