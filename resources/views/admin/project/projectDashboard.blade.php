@extends('layouts.master')

@section('title', 'Project Dashboard')

@section('content')
<h1>Projects Dashboard</h1> 

<table  class="fullTable">
    <tr>    
        <th colspan="6">
            <h2>Projects</h2>
            <button><a href="/projects/create">Add Project</a></button>
        </th>
    </tr>
    @if (isset ($projects))
    @foreach ($projects as $project)
    <tr>
        <td>
            <a href="/projects/{{ $project->id }}">{{ $project->name }}</a>
            <a class="delete" name="{{$project->id}}" href="{{ route('project.delete', $project->id) }}"><i class="material-icons">delete</i></a>
            <a class="edit" href="/projects/{{$project->id}}/edit"><i class="material-icons">edit</i></a>
        </td>
        <td>
        </td>
        <td>
            {{ $project->deadline }}
        </td>
        <td>
        @if (isset ($employers))
        @foreach ($employers as $employer)
            @if ($employer->id == '$project->employer_id')
                <p>$employer->name</p>
            @endif
        @endforeach
        @endif
        </td>
        <td>
        </td>
        <td class="completed">
            @if( $project->completed == '0')
                <p>Not Completed</p>
            @elseif( $project->completed == '1')
                <p>Completed</p>
            @endif
        </td>
    </tr>
    @endforeach
    @endif
</table>

@endsection
