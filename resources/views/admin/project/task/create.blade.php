@extends('layouts.master')

@section('title', 'New Task')

@section('content')
<h1>New Task</h1>

@if ($errors->any())
    <div class="errorAlert">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{action('TaskController@store')}}" method="post" class="createTask">
    @csrf

    <label for="name">Task Name:</label>
    <input type="text" name="name">

    <label for="description">Task Description:</label>
    <textarea name="description"></textarea>

    <label for="deadline">Deadline:</label>
    <input type="date" name="deadline">

    <input style="display: none;" name="project_id" value="{{ $project->id }}">

    <label>To be completed by:</label>
    <div class="checkboxes">
        @if (isset ($users))
        @foreach ($users as $user)

        <label for="$user_id[]">{{ $user->name }}</label>
        <input type="checkbox" name="$user_id[]" value="{{ $user->id }}">
        @endforeach
        @endif
    </div>
    <input type="submit" value="Submit">
    <button class="cancelButton"><a href="/projects/{{ $project->id }}">Cancel</a></button>

</form>
@endsection