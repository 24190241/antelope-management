@extends('layouts.master')

@section('title', 'Edit Task')

@section('content')
<h1>Edit Task</h1>

@if ($errors->any())
    <div class="errorAlert">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="/task/{{$task->id}}/update" method="PATCH"  class="editTask">
    @csrf

    <label for="name">Task Name:</label>
    <input type="text" name="name" value="{{$task->name}}">

    <label for="description">Task Description:</label>
    <textarea name="description">{{$task->description}}</textarea>

    <label for="deadline">Deadline:</label>
    <input type="date" name="deadline" value="{{$task->deadline}}">

    <input style="display: none;" name="project_id" value="{{ $project->id }}">

    <input type="submit" value="Submit">
    <button class="cancelButton"><a href="/projects/{{ $project->id }}">Cancel</a></button>

</form>
@endsection