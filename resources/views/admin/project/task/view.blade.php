@extends('layouts.master')

@section('title', 'Projects')

@section('content')

<h1>{{ $task->name }}</h1>

<h2>{{ $task->description }}</h2>

<div class="details">
    <h3>Deadline to be completed by: {{ $task->deadline }}</h3>
</div>
@endsection