@extends('layouts.master')

@section('title', 'New Project Type')

@section('content')
<h1>New Project Type</h1>

@if ($errors->any())
    <div class="errorAlert">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form action="{{action('ProjectTypeController@store')}}" method="POST" class="createProjectType">
    @csrf
    
    <label for="name">Project Type:</label>
    <input type="text" name="name">

    <input type="submit" value="Submit">
    <button class="cancelButton"><a href="/admin">Cancel</a></button>
</form>
@endsection