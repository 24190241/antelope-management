@extends('layouts.master')

@section('title', 'Edit Project Type')

@section('content')
<h1>Edit Project Type</h1>

@if ($errors->any())
    <div class="errorAlert">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form action="/projectType/{{$projectType->id}}/update" method="PATCH" class="editProjectType">
    @csrf
    
    <label for="name">Project Type:</label>
    <input type="text" name="name" value="{{$projectType->name}}">

    <input type="submit" value="Submit">
    <button class="cancelButton"><a href="/admin">Cancel</a></button>
</form>
@endsection