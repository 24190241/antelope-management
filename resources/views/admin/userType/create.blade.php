@extends('layouts.master')

@section('title', 'New User Type')

@section('content')
<h1>New User Type</h1>

@if ($errors->any())
    <div class="errorAlert">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form action="{{action('UserTypeController@store')}}" method="POST" class="createUserType">
    @csrf
    
    <label for="name">User Type:</label>
    <input type="text" name="name">

    <input type="submit" value="Submit">
    <button class="cancelButton"><a href="/admin">Cancel</a></button>
</form>
@endsection