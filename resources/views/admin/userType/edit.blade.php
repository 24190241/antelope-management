@extends('layouts.master')

@section('title', 'Edit User Type')

@section('content')
<h1>Edit User Type</h1>

@if ($errors->any())
    <div class="errorAlert">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form action="/userType/{{$userType->id}}/update" method="PATCH" class="editUserType">
    @csrf
    
    <label for="name">User Type:</label>
    <input type="text" name="name" value="{{$userType->name}}">

    <input type="submit" value="Submit">
    <button class="cancelButton"><a href="/admin">Cancel</a></button>
</form>
@endsection