@extends('layouts.master')

@section('title', 'New User')

@section('content')
<h1>New User</h1>

@if ($errors->any())
    <div class="errorAlert">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="{{action('UserController@store')}}" method="post" class="createUser">
    @csrf 

    <label for="name">Name:</label>
    <input type="text" name="name">

    <label for="email">Email Address:</label>
    <input type="email" name="email">

    <label for="jobRole">Job Role:</label>
    <input type="text" name="jobRole">

    <label for="employer_id">Employer:</label>
    <select name="employer_id">
        @if (isset ($employers))
        <option value="">Select...</option>
        @foreach ($employers as $employer)
        <option value="{{ $employer->id }}">{{ $employer->name }}</option>
        @endforeach
        @endif
    </select>

    <label for="user_type_id">User Type</label>
    <select name="user_type_id">
        @if (isset ($userTypes))
        <option value="">Select...</option>
        @foreach ($userTypes as $type)
        <option value="{{ $type->id }}">{{ $type->name }}</option>
        @endforeach
        @endif
    </select>

    <input type="submit" value="Submit">
    <button class="cancelButton"><a href="/admin">Cancel</a></button>

</form>
@endsection