@extends('layouts.master')

@section('title', 'Edit User')

@section('content')
<h1>Edit User</h1>

@if ($errors->any())
    <div class="errorAlert">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

<form action="/user/{{$user->id}}/update" method="PATCH" class="editUser">
    @csrf 

    <label for="name">Name:</label>
    <input type="text" name="name" value="{{$user->name}}">

    <label for="email">Email Address:</label>
    <input type="email" name="email" value="{{$user->email}}">

    <label for="jobRole">Job Role:</label>
    <input type="text" name="jobRole" value="{{$user->jobRole}}">

    <label for="employer_id">Employer:</label>
    <select name="employer_id">
        @if (isset ($employers))
            @foreach ($employers as $employer)
            @if($employer->id == '$user->employer_id')
                <option checked value="{{ $employer->id }}">{{ $employer->name }}</option>
            @else
                <option value="{{ $employer->id }}">{{ $employer->name }}</option>
            @endif
            @endforeach
        @endif
    </select>

    <label for="user_type_id">User Type</label>
    <select name="user_type_id">
        @if (isset ($userTypes))
            @foreach ($userTypes as $type)
                @if($type->id == '$user->user_type_id')
                    <option checked value="{{ $type->id }}">{{ $type->name }}</option>
                @else
                    <option value="{{ $type->id }}">{{ $type->name }}</option>
                @endif
            @endforeach
        @endif
    </select>

    <input type="submit" value="Submit">
    <button class="cancelButton"><a href="/admin">Cancel</a></button>

</form>
@endsection