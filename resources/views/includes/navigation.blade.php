<nav>
    <img src="{{URL('images/AntelopeManagement.png')}}" alt="">
    <ul class="topNav">
        <li><a href="/userHome">My To-Do List</a></li>
        <li><a href="/projects">Projects Dashboard</a></li>
        <li><a href="/contactBook">Contact Book</a></li>
    </ul>
    <ul class="bottomNav">
        <li><a href="/admin">Admin</a></li>
        <li><a href="">Log Out</a></li>
    </ul>
</nav>
