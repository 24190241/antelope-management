<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('/userHome', 'HomeController');
Route::resource('/contactBook', 'ContactBookController');
Route::resource('/admin', 'AdminPageController');
Route::resource('/contact', 'CompanyController');
Route::resource('/user', 'UserController');
Route::resource('/userType', 'UserTypeController');
Route::resource('/projectType', 'ProjectTypeController');
Route::resource('/employer', 'EmployerController');
Route::resource('/projects', 'ProjectController');
Route::resource('/task', 'TaskController');
Route::resource('/note', 'NoteController');
Route::resource('/document', 'DocumentController');
Route::resource('/photo', 'PhotoController');

Route::get('/projects/{id}/task/create', 'TaskController@create');
Route::get('/projects/{id}/note/create', 'NoteController@create');
Route::get('/projects/{id}/document/create', 'DocumentController@create');
Route::get('/projects/{id}/photo/create', 'PhotoController@create');


Route::get('/projects/{id}', ['as' => 'project_view', 'uses' => 'ProjectController@show']);

Route::get('/task/{id}', ['uses' => 'TaskController@show']);
Route::get('/photo/{file}', ['uses' => 'PhotoController@show']);

Route::get('/contact/{id}/edit', ['uses' => 'CompanyController@edit']);
Route::get('/employer/{id}/edit', ['uses' => 'EmployerController@edit']);
Route::get('/projectType/{id}/edit', ['uses' => 'ProjectTypeController@edit']);
Route::get('/projects/{id}/edit', ['uses' => 'ProjectController@edit']);
Route::get('/task/{id}/edit', ['uses' => 'TaskController@edit']);
Route::get('/userType/{id}/edit', ['uses' => 'UserTypeController@edit']);
Route::get('/user/{id}/edit', ['uses' => 'UserController@edit']);

Route::get('/contact/{id}/update', ['uses' => 'CompanyController@update']);
Route::get('/projects/{id}/update', ['uses' => 'ProjectController@update']);
Route::get('/projectType/{id}/update', ['uses' => 'ProjectTypeController@update']);
Route::get('/userType/{id}/update', ['uses' => 'UserTypeController@update']);
Route::get('/employer/{id}/update', ['uses' => 'EmployerController@update']);
Route::get('/user/{id}/update', ['uses' => 'UserController@update']);
Route::get('/task/{id}/update', ['uses' => 'TaskController@update']);

Route::get('note/delete/{id}', ['as' => 'note.delete', 'uses' => 'NoteController@destroy']);
Route::get('company/delete/{id}', ['as' => 'company.delete', 'uses' => 'CompanyController@destroy']);
Route::get('document/delete/{id}', ['as' => 'document.delete', 'uses' => 'DocumentController@destroy']);
Route::get('project/delete/{id}', ['as' => 'project.delete', 'uses' => 'ProjectController@destroy']);
Route::get('task/delete/{id}', ['as' => 'task.delete', 'uses' => 'TaskController@destroy']);
Route::get('user/delete/{id}', ['as' => 'user.delete', 'uses' => 'UserController@destroy']);
Route::get('photo/delete/{id}', ['as' => 'photo.delete', 'uses' => 'PhotoController@destroy']);

Route::get('document/{id}/download', ['as' => 'document.download', 'uses' => 'DocumentController@download']);

Auth::routes();
