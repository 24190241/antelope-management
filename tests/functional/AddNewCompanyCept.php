<?php 
$I = new FunctionalTester($scenario);
$I->am('officeStaff');
$I->wantTo('Add a new company to the database');

//when
$I->amOnPage('/contactBook');
$I->see('Companies', 'h2');
$I->dontSee('Ysgol Bro Alyn');
//then
$I->click('Add Company');

//then
$I->amOnPage('/contact/create');
$I->submitForm('.createContact', [
    'companyName' => 'Ysgol Bro Alyn',
    'contactName' => 'John Jones',
    'phoneNumber' => '01978485695',
    'email' => 'john@broalyn.co.uk'
]);

//then
$I->amOnPage('/contactBook');
$I->see('Companies', 'h2');
$I->See('Ysgol Bro Alyn');
