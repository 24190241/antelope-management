<?php 
$I = new FunctionalTester($scenario);
$I->am('admin');
$I->wantTo('Add a new employer to the system');

//when
$I->amOnPage('/admin');
$I->see('Employers', 'h2');
$I->dontSee('Mega Electrical');
//then
$I->click('Add Employer');
$I->submitForm('.createEmployer', [
    'name' => 'Mega Electrical'
]);
//then
$I->amOnPage('/admin');
$I->see('Employers', 'h2');
$I->see('Mega Electrical');
