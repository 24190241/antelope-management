<?php 
$I = new FunctionalTester($scenario);
$I->am('admin');
$I->wantTo('Add a new Project Type to the system');

//when
$I->amOnPage('/admin');
$I->see('Project Types', 'h2');
$I->dontSee('Tender');
//then
$I->click('Add Project Type');
$I->submitForm('.createProjectType', [
    'name' => 'Tender'
]);
//then
$I->amOnPage('/admin');
$I->see('Project Types', 'h2');
$I->see('Tender');
