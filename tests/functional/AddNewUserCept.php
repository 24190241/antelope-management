<?php 
$I = new FunctionalTester($scenario);
$I->am('admin');
$I->wantTo('Add a new user to the system');

$I->haveRecord('employers', [
    'id' => '9000',
    'name' => 'Mega Electrical'
]);
$I->haveRecord('user_types', [
    'id' => '9000',
    'name' => 'Electrician'
]);
//when
$I->amOnPage('/admin');
$I->see('System Users', 'h2');
$I->dontSee('Chris Davis');
//then
$I->click('Add User');
$I->amOnPage('/user/create');
$I->submitForm('.createUser', [
    'name' => 'Chris Davis',
    'email' => 'chris@mega.co.uk',
    'jobRole' => 'Testing engineer',
    'employer_id' => '9000',
    'user_type_id' => '9000'
]);
//then
$I->amOnPage('/admin');
$I->see('System Users', 'h2');
$I->see('Chris Davis');
