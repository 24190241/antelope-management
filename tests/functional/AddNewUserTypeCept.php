<?php 
$I = new FunctionalTester($scenario);
$I->am('admin');
$I->wantTo('Add a new user type to the system');

//when
$I->amOnPage('/admin');
$I->see('User Types', 'h2');
$I->dontSee('Electrican');
//then
$I->click('Add User Type');
$I->submitForm('.createUserType', [
    'name' => 'Electrican'
]);
//then
$I->amOnPage('/admin');
$I->see('User Types', 'h2');
$I->see('Electrican');
