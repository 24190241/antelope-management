<?php 
$I = new FunctionalTester($scenario);
$I->am('officeStaff');
$I->wantTo('Delete a company');

$I->haveRecord('companies', [
    'id' => '9000',
    'companyName' => 'Ysgol Bro Alyn',
    'contactName' => 'John Jones',
    'phoneNumber' => '1978485695',
    'email' => 'john@broalyn.co.uk'
]);

//when
$I->amOnPage('/contactBook');
$I->see('Companies', 'h2');
$I->See('Ysgol Bro Alyn');
//then
$I->click('a.delete', ['name' => '9000']);
//then
$I->amOnPage('/contactBook');
$I->see('Companies', 'h2');
$I->dontSee('Ysgol Bro Alyn');
