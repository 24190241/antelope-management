<?php 
$I = new FunctionalTester($scenario);
$I->am('officeStaff');
$I->wantTo('delete a note attatched to a project');


$I->haveRecord('companies', [
    'id' => '9000',
    'companyName' => 'Ysgol Bro Alyn',
    'contactName' => 'John Jones',
    'phoneNumber' => '1978485695',
    'email' => 'john@broalyn.co.uk'
]);
$I->haveRecord('project_types', [
    'id' => '9000',
    'name' => 'Tender'
]);
$I->haveRecord('employers', [
    'id' => '9000',
    'name' => 'A Parry Construction'
]);
$I->haveRecord('user_types', [
    'id' => '9000',
    'name' => 'officeStaff'
]);
$I->haveRecord('users', [
    'id' => '9999',
    'name' => 'Jan Williams',
    'email' => 'Jan@aparry.co.uk',
    'password' => 'password',
    'jobRole' => 'Tender Manager',
    'employer_id' => '9000',
    'user_type_id' => '9000'
]);
$I->haveRecord('projects', [
    'id' => '9000',
    'name' => 'project1',
    'description' => 'project1 description',
    'deadline' => '2023-01-01',
    'completed' => 0,
    'company_id' => '9000',
    'project_type_id' => '9000',
    'employer_id' => '9000'
]);
$I->haveRecord('notes', [
    'id' => '9000',
    'note' => 'note1',
    'user_id' => '9999',
    'project_id' => '9000'
]);

//when
$I->amOnPage('/projects/9000');
$I->see('Notes', 'h2');
$I->see('note1');
//then
$I->click('a.delete', ['name' => '9000']);
//then
$I->amOnPage('/projects/9000');
$I->see('Notes', 'h2');
$I->dontSee('note1');
