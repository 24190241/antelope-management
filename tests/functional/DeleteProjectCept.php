<?php 
$I = new FunctionalTester($scenario);
$I->am('officeStaff');
$I->wantTo('delete a project');

$I->haveRecord('companies', [
    'id' => '9000',
    'companyName' => 'Ysgol Bro Alyn',
    'contactName' => 'John Jones',
    'phoneNumber' => '1978485695',
    'email' => 'john@broalyn.co.uk'
]);
$I->haveRecord('project_types', [
    'id' => '9000',
    'name' => 'Tender'
]);
$I->haveRecord('employers', [
    'id' => '9000',
    'name' => 'A Parry Construction'
]);
$I->haveRecord('projects', [
    'id' => '9000',
    'name' => 'project1',
    'description' => 'project 1 description',
    'deadline' => '2023-01-01',
    'completed' => 0,
    'company_id' => '9000',
    'project_type_id' => '9000',
    'employer_id' => '9000'
]);
//when
$I->amOnPage('/projects');
$I->see('Projects', 'h1');
$I->see('project1');
//then
$I->click('a.delete', ['name' => '9000']);
$I->amOnPage('/projects');
$I->see('Projects', 'h1');
$I->dontSee('project1');
