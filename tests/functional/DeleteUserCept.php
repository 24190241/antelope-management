<?php 
$I = new FunctionalTester($scenario);
$I->am('admin');
$I->wantTo('delete a user');

$I->haveRecord('employers', [
    'id' => '9000',
    'name' => 'Mega Electrical'
]);
$I->haveRecord('user_types', [
    'id' => '9000',
    'name' => 'Electrician'
]);
$I->haveRecord('users', [
    'id' => '9000',
    'name' => 'Chris Davis',
    'email' => 'chris@mega.co.uk',
    'password' => 'JnR390XB',
    'jobRole' => 'Testing engineer',
    'employer_id' => '9000',
    'user_type_id' => '9000'
]);

//when
$I->amOnPage('/admin');
$I->see('System Users', 'h2');
$I->see('Chris Davis');
//then
$I->click('a.delete', ['name' => '9000']);
//then
$I->amOnPage('/admin');
$I->see('System Users', 'h2');
$I->dontSee('Chris Davis');

