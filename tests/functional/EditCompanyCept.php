<?php 
$I = new FunctionalTester($scenario);
$I->am('officeStaff');
$I->wantTo('Edit a company');

$I->haveRecord('companies', [
    'id' => '9000',
    'companyName' => 'Ysgol Bro Alyn',
    'contactName' => 'John Jones',
    'phoneNumber' => '1978485695',
    'email' => 'john@broalyn.co.uk'
]);
//when
$I->amOnPage('/contactBook');
$I->see('Companies', 'h2');
$I->see('Ysgol Bro Alyn');
//then
$I->click('a.edit');

//then
$I->amOnPage('/contact/9000/edit');
$I->submitForm('.editContact', [
    'companyName' => 'Bro Alyn',
    'contactName' => 'John Jones',
    'phoneNumber' => '1978485695',
    'email' => 'john@broalyn.co.uk'
]);

//then
$I->amOnPage('/contactBook');
$I->see('Companies', 'h2');
$I->See('Bro Alyn');