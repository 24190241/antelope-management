<?php 
$I = new FunctionalTester($scenario);
$I->am('admin');
$I->wantTo('Edit an employer');

$I->haveRecord('employers', [
    'id' => '9000',
    'name' => 'Mega Electrical'
]);
//when
$I->amOnPage('/admin');
$I->see('Employers', 'h2');
$I->see('Mega Electrical');
//then
$I->click('a.edit', ['name' => '9000']);
$I->submitForm('.editEmployer', [
    'name' => 'Mega Electrical NW LTD'
]);
//then
$I->amOnPage('/admin');
$I->see('Employers', 'h2');
$I->see('Mega Electrical NW LTD');
