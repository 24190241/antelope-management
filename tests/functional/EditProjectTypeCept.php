<?php 
$I = new FunctionalTester($scenario);
$I->am('admin');
$I->wantTo('Edit a project type');

$I->haveRecord('project_types', [
    'id' => '9000',
    'name' => 'Tender'
]);
//when
$I->amOnPage('/admin');
$I->see('Project Types', 'h2');
$I->see('Tender');
//then
$I->click('a.edit.projectType');
$I->submitForm('.editProjectType', [
    'name' => 'Pre-Tender'
]);
$I->amOnPage('/admin');
$I->see('Project Types', 'h2');
$I->see('Pre-Tender');
