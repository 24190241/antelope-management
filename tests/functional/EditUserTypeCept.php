<?php 
$I = new FunctionalTester($scenario);
$I->am('admin');
$I->wantTo('edit a user type');

$I->haveRecord('user_types', [
    'id' => '9000',
    'name' => 'Electrican'
]);
$I->amOnPage('/admin');
$I->see('User Types', 'h2');
$I->see('Electrican');
//then
$I->click('a.edit', ['name' => '9000']);
$I->submitForm('.editUserType', [
    'name' => 'Field Staff'
]);
//then
$I->amOnPage('/admin');
$I->see('User Types', 'h2');
$I->see('Field Staff');
