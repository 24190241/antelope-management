<?php 
$I = new FunctionalTester($scenario);
$I->am('officeStaff');
$I->wantTo('View a company in the system');

$I->haveRecord('companies', [
    'id' => '9000',
    'companyName' => 'Ysgol Bro Alyn',
    'contactName' => 'John Jones',
    'phoneNumber' => '1978485695',
    'email' => 'john@broalyn.co.uk'
]);

//when
$I->amOnPage('/contactBook');
$I->see('Companies', 'h2');
$I->See('Ysgol Bro Alyn');
